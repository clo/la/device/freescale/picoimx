#
# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# ARM32 device.
TARGET_ARCH := arm
TARGET_ARCH_VARIANT := armv7-a
TARGET_CPU_VARIANT := generic
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_KERNEL_ARCH := $(TARGET_ARCH)

TARGET_NO_BOOTLOADER := false
TARGET_NO_KERNEL := false
BOARD_KERNEL_BASE := 0x82800000

TARGET_USERIMAGES_USE_EXT4 := true
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_FLASH_BLOCK_SIZE := 131072

BOARD_SYSTEMIMAGE_PARTITION_SIZE := 268435456
BOARD_USERDATAIMAGE_PARTITION_SIZE := 1073741824
BOARD_CACHEIMAGE_PARTITION_SIZE := 268435456
TARGET_BOOTLOADER_BOARD_NAME := picoimx

TARGET_KERNEL_DTB := imx6ul-picosom-hobbit.dtb
BOARD_KERNEL_CMDLINE := console=ttymxc5,115200 init=/init androidboot.console=ttymxc5 androidboot.hardware=freescale cma=96M

BOARD_SEPOLICY_DIRS += device/freescale/picoimx/sepolicy

# Set up the local kernel.
TARGET_KERNEL_SRC := hardware/bsp/kernel/freescale/kernel_imx
TARGET_KERNEL_DEFCONFIG := imx_v7_android_defconfig
TARGET_KERNEL_CONFIGS := hardware/bsp/freescale/soc/picoimx/soc.kconf

# WIFI
WIFI_DRIVER_HAL_MODULE := wifi_driver.$(soc_name)
WIFI_DRIVER_HAL_PERIPHERAL := bcm4339

# $(PRODUCT_OUT) hasn't been defined yet, so use "=" instead of ":="
# so that it is resolved late.
BOARD_MKBOOTIMG_ARGS = --second $(PRODUCT_OUT)/kernel.dtb \
						--base $(BOARD_KERNEL_BASE)
